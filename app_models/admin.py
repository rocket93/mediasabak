from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(Lesson)
admin.site.register(LessonCategory)
admin.site.register(Media)
admin.site.register(ImageSlide)
admin.site.register(Test)